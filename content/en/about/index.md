---
title: "What is Jugger?"
description: ""
omit_header_text: false
featured_image: 'workshop_mai_22_bild2.jpeg'
menu:
  main:
    weight: 1
---

Jugger is an action-packed team sport that is all about tactics, fairness and fun.

This short explanatory video offers an insight:
{{< youtube H5KGov_Sajs >}}

Two teams play against each other with the goal of placing a ball (Jugg) as often as possible in the goal
(Mal) of the opposite side.

A team consists of five players. Only one of the five players, the runner, may pick
up the jugg and place it in the goal. The other four players, the pompfer, are equipped with padded sports
equipment (pompfen) with which they can tap players the opposite side. 

Players who are hit are not allowed
to participate in the game for a fixed period of time. This way the four pompfer can support their own runner
scoring.

