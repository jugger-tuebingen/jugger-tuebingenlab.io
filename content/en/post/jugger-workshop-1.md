---
date: 2022-09-06
description: ""
featured_image: "workshop_mai_22_bild1.jpeg"
tags: ["workshop"]
title: "Unisport Jugger Workshop Winter Term 22"
---

In the coming winter term we are again organizing a Jugger workshop as part of the university sports.
The workshop takes places on the 5th of November 3-6pm in the sports hall at Alberstr. 27.

[Registration](https://buchung.hsp.uni-tuebingen.de/angebote/Wintersemester_2022_2023/_Jugger.html)
