---
date: 2023-04-16
description: ""
featured_image: "course_sose23_bild1.jpeg"
tags: ["course"]
title: "Jugger Course @ Hochschulsport Summer Term 23"
---

In this summer semester there is for the first time a Jugger course in the Hochschulsport of University of Tübingen!

Registration for the course starts on Wednesday 19.4.23 at 19:50.

It will take place weekly on Wednesday 19-21h.
First date: 26.4.23

[Registration](https://buchung.hsp.uni-tuebingen.de/angebote/aktueller_zeitraum/_Jugger.html)
