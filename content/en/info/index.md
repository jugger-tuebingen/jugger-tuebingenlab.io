---
title: "Training"
description: ""
omit_header_text: false
featured_image: 'workshop_mai_22_bild2.jpeg'
menu:
  main:
    weight: 2
---

We are always happy to have new people join the training!

When you come by it is helpful to let us know via [Signal](https://signal.group/#CjQKIBc0KVpw429KP6ConszwM2YIhOx-R0HGeRiikL3mGvKIEhCLz3hYbd1G2sUkZMgy6wJf) or [Email](mailto:jugger-tuebingen@web.de) so we can bring enough pompfen.

**Training in the Hochschulsport:**

Wednesday 19 - 21h

If you want to join the Hochschulsport course please note that for insurance reasons only registered persons are allowed to participate ([link to registration](https://buchung.hsp.uni-tuebingen.de/angebote/aktueller_zeitraum/_Jugger.html)).

**Training around the [Jahnallee](https://www.openstreetmap.org/#map=19/48.51384/9.04499)**

Anytime upon arrangement in the Signal group.