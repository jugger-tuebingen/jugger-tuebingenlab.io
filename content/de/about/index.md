---
title: "Was ist Jugger?"
description: ""
omit_header_text: false
featured_image: 'workshop_mai_22_bild2.jpeg'
menu:
  main:
    weight: 1
---

Jugger ist ein actionreicher Teamsport bei dem es um Taktik, Fairness und Spielspaß geht.

Dieses kurze Erklärvideo gibt einen Einblick: 

{{< youtube H5KGov_Sajs >}}

Es spielen zwei Teams gegeneinander mit dem Ziel, einen Ball (Jugg) möglichst oft im Tor (Mal) der
Gegenseite zu platzieren. 

Ein Team besteht aus fünf Spieler:innen. Lediglich eine:r der fünf Spieler:innen,
der:die Läufer:in, darf den Jugg aufnehmen und im Mal platzieren. Die anderen vier Spieler:innen, die
Pompfer:innen, sind mit gepolsterten Sportgeräten (Pompfen) ausgestattet, mit denen sie Spieler:innen der
Gegenseite abtippen können. 

Getroffene Spieler:innen dürfen für eine festgelegte Dauer nicht mehr am Spiel
teilnehmen. So können die vier Pompfer:innen den:die eigene:n Läufer:in beim Punkten unterstützen.

