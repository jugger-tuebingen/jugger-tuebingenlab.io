---
title: "Training"
description: ""
omit_header_text: false
featured_image: 'workshop_mai_22_bild2.jpeg'
menu:
  main:
    weight: 2
---

Wir freuen uns immer über neue Leute im Training!

Wenn ihr vorbeikommt ist es hilfreich über [Signal](https://signal.group/#CjQKIBc0KVpw429KP6ConszwM2YIhOx-R0HGeRiikL3mGvKIEhCLz3hYbd1G2sUkZMgy6wJf) oder [Email](mailto:jugger-tuebingen@web.de) Bescheid zu geben, damit wir genug Pompfen mitbringen.


**Training im Hochschulsport:**

Mittwoch 19 - 21 Uhr

Wer beim Hochschulsport-Kurs dabei sein möchte beachte bitte, dass aus versicherungstechnischen Gründen nur angemeldete Personen teilnehmen dürfen ([Link zur Anmeldung](https://buchung.hsp.uni-tuebingen.de/angebote/aktueller_zeitraum/_Jugger.html)).


**Training bei der [Jahnallee](https://www.openstreetmap.org/#map=19/48.51384/9.04499)**

Jederzeit nach Absprache in der Signal-Gruppe.
