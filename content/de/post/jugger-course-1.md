---
date: 2023-04-16
description: ""
featured_image: "course_sose23_bild1.jpeg"
tags: ["course"]
title: "Jugger Kurs @ Hochschulsport SoSe23"
---

In diesem Sommersemester gibt es zum ersten mal einen Jugger Kurs im Hochschulsport der Uni Tübingen!

Anmeldestart für den Jugger Kurs ist am Mittwoch 19.4.23 ab 19:50 Uhr

Der Kurs findet wöchentlich Mittwoch 19-21 Uhr statt.
Erster Termin: 26.4.23

[Zur Anmeldung](https://buchung.hsp.uni-tuebingen.de/angebote/aktueller_zeitraum/_Jugger.html)
