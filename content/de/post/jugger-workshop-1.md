---
date: 2022-09-06
description: ""
featured_image: "workshop_mai_22_bild1.jpeg"
tags: ["workshop"]
title: "Unisport Jugger Workshop WiSe22"
---

Im kommenden Wintersemester wird es wieder einen Jugger Workshop im Rahmen des Unisports geben.
Der Workshop findet am 5.11.22 von 15-18 Uhr in der Sporthalle Alberstr. 27 statt.

[Zur Anmeldung](https://buchung.hsp.uni-tuebingen.de/angebote/Wintersemester_2022_2023/_Jugger.html)
